import test from 'tape'
import xhtm from 'xhtm'

import vhtml from './index.js'
const html = xhtm.bind(vhtml)

test('vhtml', t => {
  t.equal(
    html`
      <p>
        ${`<script>alert("hello" + ', world!')</script>`}
      </p>
    `,
    `<p>&lt;script&gt;alert(&quot;hello&quot; + &apos;, world!&apos;)&lt;/script&gt;</p>`,
    'should not allow code injection'
  )

  t.equal(
    html`
      <script>
        alert("hello" + ', world!')
      </script>
    `,
    `<script>alert("hello" + ', world!')</script>`,
    'should not escape contents of script tags'
  )

  t.equal(
    html`
      <style>
        ${'</style><script>alert("hello")</script>'}
      </style>
    `,
    '<style>&lt;/style><script>alert("hello")&lt;/script></style>',
    'should not allow code injection into style tags'
  )

  t.equal(
    html`
      <style>
        p::before {
          content: "before"
        }
        p::after {
          content: 'after'
        }
      </style>
    `,
    `<style>p::before {content: "before"}p::after {content: 'after'}</style>`,
    'should render quotes correctly in style tags'
  )

  // Sanitise cache injection.
  // The issue at https://github.com/developit/vhtml/issues/34
  // does not work for script injection attacks.

  const scriptInjectionAttempt = '<script>alert("hello")</script>';

  const firstTry = html`<div>${scriptInjectionAttempt}</div>`
  const secondTry = html`<div>${'<script>alert("hello")</script>'}</div>`
  const thirdTry = html`<div>${scriptInjectionAttempt}</div>`

  const expectedResult = '<div>&lt;script&gt;alert(&quot;hello&quot;)&lt;/script&gt;</div>'

  t.equal(firstTry, expectedResult, 'should sanitise uncached script injection attempt')
  t.equal(secondTry, expectedResult, 'should sanitise script injection attempt on initial cache hit')
  t.equal(thirdTry, expectedResult, 'should sanitise script injection attempt subsequence cache hits')

  // Should stringify HMTL.

  let items = ['one', 'two', 'three']
  
  t.equal(
    html`
      <div class="foo">
        <h1>Hi!</h1>
        <p>Here is a list of ${items.length} items:</p>
        <ul>
          ${
            items.map( item => (
              html`<li>${ item }</li>`
            ))
          }
        </ul>
      </div>
    `,
    '<div class="foo"><h1>Hi!</h1><p>Here is a list of 3 items:</p><ul><li>one</li><li>two</li><li>three</li></ul></div>',
    'should stringify html'
  )

  t.equal(
    html`
      <div>
        ${ `<strong>blocked</strong>` }
        <em>allowed</em>
      </div>
    `,
    `<div>&lt;strong&gt;blocked&lt;/strong&gt;<em>allowed</em></div>`,
    'should sanitise children'
  )

  t.equal(html`<div onclick=${`&<>"'`}></div>`, '<div onclick="&amp;&lt;&gt;&quot;&apos;"></div>', 'should sanitise attributes')

  t.equal(
    html`<div dangerouslySetInnerHTML=${{ __html: "<span>Injected HTML</span>" }} />`,
    '<div><span>Injected HTML</span></div>',
    'should NOT sanitise dangerouslySetInnerHTML attribute and directly set its __html property as innerHTML'
  )

  t.equal(
    html`<style>p::before { content: 'something'; }</style>`,
    `<style>p::before { content: 'something'; }</style>`,
    'should not sanitise <style> tags'
  )

  t.equal(
    html`<p hidden>I am hidden.</p>`,
    '<p hidden>I am hidden.</p>',
    'should correctly emit boolean HTML attributes'
  )

  t.equal(
    html`
      <div>
        ${[['a','b']]}
        <c>d</c>
        ${['e',['f'],[['g']]]}
      </div>
    `,
    '<div>ab<c>d</c>efg</div>',
    'should flatten children'
  )

  items = ['one', 'two']

  let Item = ({ item, index, children }) => html`
    <li id=${index}>
      <h4>${item}</h4>
      ${children}
    </li>
  `
  
  t.equal(
    html`
      <div class="foo">
        <h1>Hi!</h1>
        <ul>
          ${
            items.map((item, index) => html`
              <${Item} item=${item} index=${index}>
                This is item ${item}!
              </${Item}>
            `)
          }
        </ul>
      </div>
    `,
    '<div class="foo"><h1>Hi!</h1><ul><li id="0"><h4>one</h4>This is item one!</li><li id="1"><h4>two</h4>This is item two!</li></ul></div>',
    'should support sort-of components'
  )

  Item = () => html`
    <li>
      <h4></h4>
    </li>
  `

  t.equal(
    html`
      <div class="foo">
        <h1>Hi!</h1>
        <ul>
          ${
            items.map(item => html`
              <${Item}>
                This is item ${item}!
              </${Item}>
            `)
          }
        </ul>
      </div>
    `,
    '<div class="foo"><h1>Hi!</h1><ul><li><h4></h4></li><li><h4></h4></li></ul></div>',
    'should support sort-of components without arguments'
  )  

  Item = ({ children }) => html`
    <li>
      <h4></h4>
      ${children}
    </li>
  `

  t.equal(
    html`
      <div class="foo">
        <h1>Hi!</h1>
        <ul>
          ${
            items.map(item => html`
              <${Item}>
                This is item ${item}!
              </${Item}>
            `)
          }
        </ul>
      </div>
    `,
    '<div class="foo"><h1>Hi!</h1><ul><li><h4></h4>This is item one!</li><li><h4></h4>This is item two!</li></ul></div>',
    'should support sort-of components without args but with children'
  )  

  t.equal(
    html`
      <div>
        <area />
        <base />
        <br />
        <col />
        <command />
        <embed />
        <hr />
        <img />
        <input />
        <keygen />
        <link />
        <meta />
        <param />
        <source />
        <track />
        <wbr />
        <!-- Not void elements -->
        <div />
        <span />
        <p />
      </div>
    `,
    '<div><area><base><br><col><command><embed><hr><img><input><keygen><link><meta><param><source><track><wbr><div></div><span></span><p></p></div>',
    'should support empty (void) tags'
  )

  t.equal(html`<div className="my-class" htmlFor="id" />`, '<div class="my-class" for="id"></div>', 'should handle special prop names')

  t.equal(vhtml(null, null, "foo", "bar", "baz"), 'foobarbaz', 'should support string fragments')

  t.equal(vhtml(null, null, html`<p>foo</p>`, html`<em>bar</em>`, html`<div class="qqqqqq">baz</div>`), '<p>foo</p><em>bar</em><div class="qqqqqq">baz</div>', 'should support element fragments')

  t.end()
})
