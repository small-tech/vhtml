# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] - 2023-03-03

Sanitisation.

### Changed

  - Contents of `<script>` tags are not sanitised (script tags in interpolated values are, of course, sanitised.)
    
    This is not a breaking change as, say, if previously you were passing values to `xhtm` using the `raw()` function in [Kitten](https://codeberg.org/kitten/app), your code will still work (it’ll just include an unnecessary `dangerouslySetInnerHTML` call and you can remove the call to `raw()` and it should function equivalently.)

### Added

  - Test for sanisation of `<script>` tags in interpolated values.
  - Test for sanisation of `<script>` tags in `<style>` tags (edge case).

## [1.0.0] - 2023-03-02

A fresh start.

### Changed

  - Forked from https://github.com/developit/vhtml
  - Changed npm package name to @small-tech/vhtml
  - Reset version to 1.0.0 for new fork.
  - Style tags are no longer sanitised (a691bdba8c0e8541028c8c5ac48ff3c7a1aaac39)
  - Boolean HTML attributes are emitted correctly; without `="true"` (1c6698ff56d888ec0e7dbeb4bc971393339b7364)
  - Source is now in the main directory, in a single file.
  - Tests are now in tape with tap-monkey reporter instead of mocha and chai.
  - Tests now use [xhtm](https://github.com/dy/xhtm) instead of JSX.
  - Tests are now in the main directory, in a single file.

### Added

  - Changelog.

### Removed

  - 18 development dependencies including babel, rollup, and uglify.
