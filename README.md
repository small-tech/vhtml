# vhtml

## Fork of vhtml optimised for Kitten

Fork of [vhtml by Jason Miller](https://github.com/developit/vhtml) optimised for HTML instead of JSX (mainly for use by [Kitten](https://codeberg.org/kitten/app)).

If you’re wondering which module to use, you probably should use Jason’s original one.

## What’s different

The differences are mostly due to Kitten’s tagged templates targetting HTML rather than JSX.

  - Boolean HTML attributes are output as just the name of the attribute, not `attribute="true"` (e.g.,`h('main', {hidden: true})` is rendered as `<div hidden></div>`, not `<div hidden="true"></div>`)

  - `<style>…</style>` tags are not escaped, so you can use quotation marks in them.

Other differences:

  - Moved repository to Codeberg.
  - Changed npm package name to @small-tech/vhtml
  - Reset version to 1.0.0 for new fork.
  - Made the module ESM-only to keep things simple.
  - Source is now in the main directory, in a single file.
  - Tests are now in [tape](https://github.com/ljharb/tape) with [tap-monkey](https://github.com/small-tech/tap-monkey) reporter instead of mocha and chai.
  - Tests now use [xhtm](https://github.com/dy/xhtm) instead of JSX.
  - Tests are now in the main directory, in a single file.
  - Removed 18 development dependencies including babel, rollup, and uglify.

For full list of changes, please [see the changelog](https://codeberg.org/small-tech/vhtml/src/branch/master/CHANGELOG.md).

## Installation

Via npm:

`npm install @small-tech/vhtml`

## Usage

See [vhtml](https://github.com/developit/vhtml)

## License

MIT (as per the original module)
