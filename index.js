// List of HTML tags that don’t have content.
const emptyTags = [
  'area',
  'base',
  'br',
  'col',
  'command',
  'embed',
  'hr',
  'img',
  'input',
  'keygen',
  'link',
  'meta',
  'param',
  'source',
  'track',
  'wbr'
]

// Escaping.
const map = {'&':'amp','<':'lt','>':'gt','"':'quot',"'":'apos'}
const setInnerHTMLAttr = 'dangerouslySetInnerHTML'

const escapeAttributes = str => String(str).replace(/[&<>"']/g, s => `&${map[s]};`)
const escapeEndTags = str => String(str).replace(/<\//g, '&lt;/')

const DOMAttributeNames = {
  className: 'class',
  htmlFor: 'for'
}

// Housekeeping.

// Based on https://github.com/developit/vhtml/pull/36/files
// Fixes https://github.com/developit/vhtml/issues/20
let cleanupTimer = null 
const isSanitised = new Set()

function cleanup () {
  isSanitised.clear()
  cleanupTimer = null
}

function scheduleCleanup () {
  if (cleanupTimer === null) cleanupTimer = setImmediate(cleanup)
}

/** Hyperscript reviver that constructs sanitised HTML string. */
export default function h(name, attrs) {
  let stack=[], s = ''
  attrs = attrs || {}
  for (let i=arguments.length; i-- > 2; ) {
    stack.push(arguments[i])
  }

  // Sortof component support!
  if (typeof name==='function') {
    attrs.children = stack.reverse()
    return name(attrs)
    // return name(attrs, stack.reverse())
  }

  if (name) {
    s += '<' + name
    if (attrs) for (let i in attrs) {
      if (attrs[i]!==false && attrs[i]!=null && i !== setInnerHTMLAttr) {
        // Note the additional check for boolean attribute values e.g.,
        // h('p', {hidden: true}) which should render as <p hidden> not <p hidden="true">
        s += ` ${DOMAttributeNames[i] ? DOMAttributeNames[i] : escapeAttributes(i)}${attrs[i] === true ? '' : `="${escapeAttributes(attrs[i])}"`}`
      }
    }
    s += '>'
  }

  if (emptyTags.indexOf(name) === -1) {
    if (attrs[setInnerHTMLAttr]) {
      s += attrs[setInnerHTMLAttr].__html
    }
    else while (stack.length) {
      let child = stack.pop()
      if (child) {
        if (child.pop) {
          for (let i=child.length; i--; ) stack.push(child[i])
        }
        else {
          // Note: we exempt <style> tags from sanitisation for CSS
          // that contains quotation marks, etc., to work. To avoid the
          // possibility of script injection, we do, however,
          // escape closing tags (</).
          s += isSanitised.has(child) ? child : (name === 'style' || name === 'script' ? escapeEndTags(child) : escapeAttributes(child))
        }
      }
    }

    s += name ? `</${name}>` : ''
  }

  // console.log('-->', s)
  isSanitised.add(s)
  scheduleCleanup()
  return s
}
